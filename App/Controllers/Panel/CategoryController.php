<?php
namespace App\Controllers\Panel;

use App\Core\Request;
use App\Repositories\AttributeRepo;
use App\Repositories\CategoryRepo;
use App\Services\Flash\FlashMessage;
use App\Services\View\View;

class CategoryController{

	public function index(Request $request) {
        $repo = new CategoryRepo();
        $data = [
            'cats' => $repo->all()
        ];
        View::load('panel.category.index',$data,'panel-admin');
	}
	
	public function attributes(Request $request) {
        $catRepo = new CategoryRepo();
        $attrRepo = new AttributeRepo();
        $data = [
            'cats' => $catRepo->all(),
            'attrs' => $attrRepo->all(),
            'catAttrs' => []
        ];
        if(isset($_GET['cat']) and is_numeric($_GET['cat'])){
            $data['catAttrs'] = $catRepo->getAttributeIds($_GET['cat']);
        }

        View::load('panel.category.attributes', $data, 'panel-admin');
    }

    public function create(Request $request)
    {
        // input validation & filtering here
        $vRes = true ;   // result of validation and filtering
        if ($vRes === true) {
            $repo = new CategoryRepo();
            $repo->create($request->except(['csrf']));
            FlashMessage::add("انجام شد", FlashMessage::SUCCESS);
        }
        Request::redirect('panel/categories');
    }

    public function modifyAttributes(Request $request)
    {
        // input validation & filtering here
        $vRes = true ;   // result of validation and filtering
        if ($vRes === true) {
            $repo = new CategoryRepo();
            $repo->setAttributes($request->param('category_id'),$request->param('selectedAttrs'));
            FlashMessage::add("انجام شد", FlashMessage::SUCCESS);
        }
        Request::redirect('panel/categories/attributes');
    }

}