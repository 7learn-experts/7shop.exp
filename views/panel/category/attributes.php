<div class="page-wrapper ssWrap">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card p-30">
                    <form action="<?php echo siteUrl('panel/categories/modify-attributes'); ?>" method="post">
                        <?= csrf_field(); ?>
                        <div class="form-group">
                            <h4>انتخاب دسته بندی</h4>
                            <select name="category_id" class="form-control" id="catSelect">
                                <option value="0">دسته بندی اصلی</option>
                                <?php foreach ($cats as $cg): ?>
                                    <option value="<?= $cg->id ?>" <?php echo (isset($_GET['cat']) and  $_GET['cat'] == $cg->id) ? "selected" : ""; ?>><?= $cg->title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <h4>انتخاب ویژگی ها</h4>

                            <?php foreach ($attrs as $attr): ?>
                                <label for="attr<?= $attr->id ?>" class="inlineLabel">
                                <input type="checkbox" name="selectedAttrs[]" id="attr<?= $attr->id ?>" value="<?= $attr->id ?>" <?= in_array($attr->id, $catAttrs) ? "checked" : "" ?>> <?= $attr->title ?>
                                </label>
                            <?php endforeach; ?>
                        </div>

                        <button type="submit" class="btn btn-info btn-flat btn-addon m-b-10">افزدن</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
