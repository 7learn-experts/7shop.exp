<?php

namespace App\Controllers;

use App\Core\Request;
use App\Services\View\View;
use App\Utility\Cart;
use App\Repositories\ProductRepo;
use App\Repositories\OrderRepo;
use App\Services\Auth\Auth;

class OrderController
{
    public function cart(Request $request)
    {
        $cart_items =  Cart::items();
        $productRepo = new ProductRepo();

        $data = [
            "cart_items" => $cart_items,
            "cart_products" => $productRepo->findMany(array_keys($cart_items))
        ];
        
        View::load('order.cart', $data);
    }

    public function verify(Request $request)
    {
        $data = [];
        $orderRepo = new OrderRepo();
        $currentUser = Auth::current_user();
        // form validation
        // check if user logged in -> continue ; else -> redirect to auth/login
        if (!Auth::user_logged_in()) {
            Request::redirect("auth/login");
        }
        // verify and insert order
        $order = $orderRepo->create([
            'user_id' => $currentUser->id,
            'total_items' => sizeof($request->param('productCount')),
            'total_amount' => $request->param('total_amount')
            ]);

        // insert order products
        $orderRepo->saveProducts($order, $request->param('productCount'));

        // homeWork :
        // add User Address
        // add a shipment for order

        // clear shopping cart
        Cart::clear();

        // add a payment(status started)

        // redirect to gateway ...
        View::load('order.verify', $data);
    }
}
