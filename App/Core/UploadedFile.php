<?php

namespace App\Core;

class UploadedFile {
	private $file;

	public function __construct($fileName ) {
		$this->file = $_FILES[ $fileName ];
	}

	public function mimeType() {
		return $this->file['type'];
	}

	public function size() {
		return $this->file['size'];
	}

    public function name()
    {
        return $this->file['name'];
    }

    public function extension() {
        $arr = explode( '.', $this->name() );
        return '.' . end( $arr );
    }

    public function basename()
    {
        return basename($this->name(), $this->extension());
    }


	private function generateRandomStr() {
		return bin2hex(random_bytes(4));
	}

    private function upload($path)
    {
        return move_uploaded_file($this->file['tmp_name'], $path);
    }

    public function save() {
		$newFileName = $this->basename() . '-' . $this->generateRandomStr() . $this->extension();
		$path        = UPLOAD_PATH . $newFileName;
		if ( $this->upload( $path ) ) {
            $file_url = UPLOAD_BASE_URL . $newFileName;
            return $file_url;
		}
		return null;
	}
}
