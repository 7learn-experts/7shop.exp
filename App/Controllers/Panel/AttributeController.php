<?php
namespace App\Controllers\Panel;

use App\Core\Request;
use App\Repositories\AttributeRepo;
use App\Services\Flash\FlashMessage;
use App\Services\View\View;

class AttributeController{

	public function index($request) {
        $repo = new AttributeRepo();
        $data = [
            'attrs' => $repo->all()
        ];
        View::load('panel.attribute.index',$data,'panel-admin');
	}

    public function create(Request $request)
    {
        // input validation & filtering here
        $vRes = true;   // result of validation and filtering
        if ($vRes === true) {
            $repo = new AttributeRepo();
            $repo->create($request->except(['csrf']));
            FlashMessage::add("ویژگی با موفقیت اضافه شد", FlashMessage::SUCCESS);
        }
        Request::redirect('panel/attributes');
    }
}