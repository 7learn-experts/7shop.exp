<div class="page-wrapper ssWrap">
    <div class="container-fluid">
        <div class="row">
            <form action="<?php echo siteUrl('panel/product/save'); ?>" method="post" style="width: 100%">
            <div class="col-lg-12">
                <div class="card p-30">
                    <div class="card-title">
                        <h4>افزودن محصول جدید</h4>
                    </div>
                        <?= csrf_field(); ?>
                        <div class="form-group">
                            <h4>انتخاب دسته بندی محصول</h4>
                            <select name="category_id" class="form-control" id="catSelect">
                                <option value="0">دسته بندی اصلی</option>
                                <?php foreach ($cats as $cg): ?>
                                    <option value="<?= $cg->id ?>" <?php echo (isset($_GET['cat']) and $_GET['cat'] == $cg->id) ? "selected" : ""; ?>><?= $cg->title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                </div>
                </div>
                <?php if (isset($_GET['cat']) and is_numeric($_GET['cat'])): ?>
                <div class="col-lg-12">
                <div class="card p-30">
                    <div class="card-title">
                        <h4>مشخصات محصول</h4>
                    </div>
                    <div class="form-group">
                                <label>عنوان محصول</label>
                                <input type="text" name="title" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label>توضیحات محصول</label>
                                <textarea class="form-control" name="description" rows="3" placeholder="مقدار تظیمات رو وارد کنید"></textarea>
                            </div>
                            <div class="form-group">
                                <label>قیمت محصول به ریال</label>
                                <input type="text" name="price" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label>تعداد موجودی</label>
                                <input type="number" name="quantity" class="form-control" value="">
                            </div>
                </div>
                </div>
                <div class="col-lg-12">
                    <div class="card p-30">
                        <div class="card-title">
                            <h4> ویژگی های محصول</h4>
                        </div>
                        <div class="form-group">
                            <?php foreach ($attrs as $attr): ?>
                            <?= getFormField($attr); ?>
                            <?php endforeach; ?>
                        </div>
                        <button type="submit" class="btn btn-info btn-flat btn-addon m-b-10">افزدن</button>
                        <?php endif; ?>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
