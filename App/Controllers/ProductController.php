<?php

namespace App\Controllers;

use App\Core\Request;
use App\Services\View\View;
use App\Repositories\ProductRepo;
use App\Utility\Cart;

class ProductController
{
    public function index(Request $request)
    {
        if (empty($request->param('id') || !is_numeric($request->param('id')))) {
            Request::redirect();
        }
        if ($request->isPost() && is_numeric($request->param('item'))) {
            // set cookie
            Cart::add($request->param('item'), $request->param('count'));
        }
        
        $prodectRepo = new ProductRepo();
        $prodect = $prodectRepo->find($request->param('id'));
        
        // Homework : get product media & attributes and display in view
        $media = [];
        $attrs = [];

        $data = [
            'product' => $prodect,
            'media' => $media,
            'attrs' => $attrs,
        ];

        View::load('product.index', $data);
    }
}
