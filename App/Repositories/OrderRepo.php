<?php
namespace App\Repositories;

use App\Models\Order;

class OrderRepo extends BaseRepo
{
    protected $model = Order::class;

    public function saveProducts($order, $products_data)
    {
        $orderProductRepo = new OrderProductRepo();
        $productRepo = new ProductRepo();
        // homework : use createMany() here !
        foreach ($products_data as $product_id=>$product_count) {
            $product = $productRepo->find($product_id);
            $orderProductRepo->create([
                "order_id" => $order->id,
                "product_id" => $product_id,
                "quantity" => $product_count,
                "single_price" => $product->price,
            ]);
        }
    }
}
