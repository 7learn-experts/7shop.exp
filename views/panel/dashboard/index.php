<!-- Page wrapper  -->
<div class="page-wrapper ssWrap ss-dashboard">
    <!-- Container fluid  -->
    <div class="container-fluid">

        <div class="row faNum">
            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-usd f-s-40 color-primary"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2>5000 تومان</h2>
                            <p class="m-b-0">کل درامد</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-shopping-cart f-s-40 color-success"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2>125</h2>
                            <p class="m-b-0">فروش</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-archive f-s-40 color-warning"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2>256</h2>
                            <p class="m-b-0">سفارش</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-user f-s-40 color-danger"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h2>2381</h2>
                            <p class="m-b-0">کاربر</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>دکمه ها و المنت های کاربردی</h4>
                    </div>
                    <div class="card-body">
                        <button type="button" class="btn btn-danger btn-flat btn-addon btn-xs m-b-10">دکمه</button>
                        <button type="button" class="btn btn-success btn-flat btn-addon btn-xs m-b-10">دکمه</button>
                        <button type="button" class="btn btn-info btn-flat btn-addon btn-xs m-b-10">دکمه</button>
                        <button type="button" class="btn btn-primary btn-flat btn-addon btn-xs m-b-10">دکمه</button>
                        <span class="badge">وضعیت</span>
                        <span class="badge badge-primary">وضعیت</span>
                        <span class="badge badge-success">وضعیت</span>
                        <span class="badge badge-info">وضعیت</span>
                        <span class="badge badge-warning">وضعیت</span>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>فرم و فیلدهای پایه ای</h4>
                    </div>
                    <div class="card-body">
                        <div class="basic-elements">
                            <form>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>متن</label>
                                            <input type="text" class="form-control" value="متن">
                                        </div>
                                        <div class="form-group">
                                            <label>ایمیل</label>
                                            <input id="example-email" class="form-control" type="email"
                                                   placeholder="ایمیل شما" name="example-email">
                                        </div>
                                        <div class="form-group">
                                            <label>رمز عبور</label>
                                            <input class="form-control" type="password" value="password">
                                        </div>
                                        <div class="form-group">
                                            <label>متن بزرگ textarea</label>
                                            <textarea class="form-control" rows="3" placeholder="بنویسید"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>فیلد Readonly</label>
                                            <input class="form-control" type="text" value="فقط خواندنی" readonly>
                                        </div>

                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Disabled</label>
                                            <input class="form-control" type="text" value="Disabled value" disabled="">
                                        </div>
                                        <div class="form-group">
                                            <label>متن توضیحی</label>
                                            <input class="form-control" type="text" placeholder="Helping text">
                                            <span class="help-block">
                                            <small>متن زیر نویس یک فیلد</small>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <label>انتخاب یک گزینه</label>
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>انتخاب چند گزینه</label>
                                            <select class="form-control mh100" multiple>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# column -->
        </div>
        <div class="row templates">
            <div class="col-lg-4 ttable">
                <div class="card">
                    <div class="card-title">
                        <h4>لیست کاربران </h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>عنوان</th>
                                    <th>عنوان</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td scope="row">1</td>
                                    <td>مقدار</td>
                                    <td>
                                        <span class="badge badge-primary">وضعیت</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 tchart1">
                <div class="card">
                    <div class="card-title">
                        <h4>آمار فروش</h4>

                    </div>
                    <div class="sales-chart">
                        <canvas id="sales-chart"></canvas>
                    </div>
                </div>
            </div>
            <!-- /# column -->
            <div class="col-lg-4 tchart2">
                <div class="card">
                    <div class="card-title">
                        <h4>آمار کاربران ثبت نامی</h4>

                    </div>
                    <div class="sales-chart">
                        <canvas id="team-chart"></canvas>
                    </div>
                </div>
                <!-- /# card -->
            </div>
        </div>

        <!-- /# row -->

        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
<!-- End Page wrapper  -->