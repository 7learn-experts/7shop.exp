<?php
namespace App\Repositories;

use App\Models\OrderProduct;

class OrderProductRepo extends BaseRepo
{
    protected $model = OrderProduct::class;
}
