<?php
namespace App\Models;

class Product extends BaseModel
{
    protected $table = 'products' ;
    protected $primaryKey = 'id' ;

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function medias()
    {
        return $this->hasMany(Media::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_products', 'product_id', 'order_id');
    }

    public function getCategoryTitleAttribute()
    {
        return $this->category->title;
    }
}
