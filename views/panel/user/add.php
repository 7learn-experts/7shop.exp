<div class="page-wrapper ssWrap">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card p-30">
                    <form action="<?= siteUrl('panel/users/create'); ?>" method="post">
                        <?= csrf_field(); ?>
                        <div class="form-group">
                            <label>نام کامل</label>
                            <input type="text" name="fullname" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label>ایمیل</label>
                            <input type="text" name="email" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label>پسورد</label>
                            <input type="password" name="password" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label>نقش کاربری</label>
                            <select name="role" class="form-control">
                                <option value="user">کاربر</option>
                                <option value="agent">ایجنت</option>
                                <option value="admin">مدیر</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-info btn-flat btn-addon m-b-10">افزدن</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
